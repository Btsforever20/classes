#include <iostream>
#include "OnlineVid.h"


using namespace std;

OnlineVid::OnlineVid()
{
	views=0;
	id=0;

}

OnlineVid::OnlineVid(unsigned val, int var)
{
	views=val;
	id=var;
}

 void OnlineVid::setViews(unsigned val )
 {
   views=val;
 }

 unsigned OnlineVid::getViews()
 {
  return views;
 }
 
 
 void OnlineVid::setId( int var)
 {
	 id=var;
 }
 
 int OnlineVid::getId()
 {
	 return id;
 }
